# DM Conception logicielle

Voici le dépot du devoir maison pour le cours de conception logicielle à l'ENSAI. 

## Choix du sujet : Webservice d'envoi de mail

Il s'agît d'un webservice pour programmer l'envoi d'un mail. 

### Description de l'application

```mermaid
graph TD;
    User[Utilisateur] -->|Python| App;
    App[app] -->|HTTP| Api;
    Api[my API] -->|Psycopg| BDD;
    App[app] -->|API smtplib| SMTP;
```


### Installation

Pour installer l'application, vous pouvez cloner ce dépot : 
```
$ git clone https://gitlab.com/Chaumette35/dm-conception-logicielle.git
$ cd dm-conception-logicielle/app
$ pip install -r requirements.txt
```

### Prérequis

Les prérequis pour utiliser cette application sont : 

- Il vous faut la version 3.10 ou supérieure de python. 
- Vous devez avoir une base de données SQL pour stocker vos demandes de mail. Pour la configuration, dans le fichier `.env`, vous devez modifier les différents paramètres de la base de données. 
- Dans cette base de données, vous exécuterez le fichier `init_db.sql` pour créer les différentes tables nécessaires à l'application. 
- Ici, il y a deux cas possibles en fonction du choix de serveur SMTP : 
    - Si vous avez un serveur SMTP local, alors vous devez renseigner son `host`et son `port`dans le fichier `.env`. 
    - Si vous voulez utiliser un serveur SMTP comme celui de google, alors vous devez renseigner l'`host`du serveur, votre e-mail et votre mot de passe dans le fichier `.env`. 

### Utiliser l'application

Les différentes étapes pour pouvoir utiliser ce webservice sont : 

- Etape 1 : dans un premier terminal, lancer l'API utilisée pour intéragir avec la base de données grâce à la commande `$ python3 start_api.py`
- Etape 2 : dans un deuxième terminal, lancer l'application qui enverra les mails aux heures convenues. Pour cela, lancer `$ python3 start_app.py`
- Etape 3 : si vous avez un serveur SMTP local, vous devez le mettre en marche. Par exemple, vous pouvez faire `$ python3 -m smtpd -c DebuggingServer -n localhost:1025`
- Etape 4 : Dans un dernier terminal, lancer la commande suivante pour faire les demandes d'envoi de mail `$ python3 main.py`

!! ATTENTION !!

Vous ne pouvez pas envoyer de mails qui ont des caractères non-ASCII

### Tester l'application

Pour tester l'application, vous pouvez utiliser la commande `$ python3 -m unittest discover tests -p "test_*.py"`