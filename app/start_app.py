from model.send_demande import SendDemande

import time


print("Merci beaucoup d'utiliser l'application !")
nb_mail_send = 0

try:
    while True:

        nb_mail_send += SendDemande().checking_date()
        time.sleep(30)

except KeyboardInterrupt:
    pass
finally:
    print("\nAu revoir")
    if nb_mail_send <= 1:
        fin_message = " mail a été envoyé grâce à l'application"
    else:
        fin_message = " mails ont été envoyés grâce à l'application"
    print("Au total, " + str(nb_mail_send) + fin_message)