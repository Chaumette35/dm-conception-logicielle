import json

from dao.db_connection import DBConnection
from model.demande import Demande

class DemandeDao:
    """Permet l'accès à la table demande de la base de données"""

    def find_demandes(self) -> Demande:
        """Méthode qui renvoie l'ensemble des demandes effectuées
        
        Returns
        list[Demande]
        """
        request = "SELECT * FROM conception_logicielle.demande;"

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(request)
                res = cursor.fetchall()

        demandes = []
        if res:
            for row in res:
                demande = Demande(id_demande = row['id_demande'], mail = row['mail'], isSending=row['issending'])
                demandes.append(demande)

        return demandes


    def find_demande_id(self, id_demande: int) -> Demande:
        """Méthode qui renvoie la demande associée à son identifiant
        
        Parameters
        ----------
        id_demande: identifiant associé à la demande recherchée
            int
        
        Returns
        -------
        Demande
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM conception_logicielle.demande "\
                    "WHERE id_demande = %(id_demande)s;"
                    , {"id_demande": id_demande}
                )
                res = cursor.fetchone()

        if res:
            demande = Demande(id_demande = res['id_demande'], mail = res['mail'], isSending=res['issending'])
        else:
            demande = None

        return demande


    def add_demande(self, demande: Demande) -> Demande:
        """Ajoute une demande à la base de données
        
        Parameters
        ----------
        demande: Demande ajoutée à la base de données
            Demande
        
        Returns
        -------
        Demande
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO conception_logicielle.demande (id_demande, mail, isSending) "\
                    "VALUES (NEXTVAL('conception_logicielle.sequence_demande'), %(mail)s, %(isSending)s) "\
                    "RETURNING id_demande;"
                    , {"mail": json.dumps(demande.mail), "isSending": demande.isSending}
                )
                res = cursor.fetchone()

        if res:
            demande.id_demande = res['id_demande']

        return demande


    def send_demande(self, id_demande: int) -> None:
        """Méthode qui modifie une demande car celle-ci a été envoyée
        
        Parameters
        ----------
        id_demande: identifiant associée à la demande qui à été envoyée
            int
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE conception_logicielle.demande "\
                    "SET issending = true "\
                    "WHERE id_demande = %(id_demande)s;"
                    , {'id_demande': id_demande}
                )


    def find_demande_not_send(self):
        """Méthode qui renvoie l'ensemble des demandes qui n'ont pas encore été envoyées
        
        Returns
        -------
        list[Demande]
        """
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM conception_logicielle.demande "\
                    "WHERE isSending = false"
                )
                res = cursor.fetchall()

        demandes = []
        if res:
            for row in res:
                demande = Demande(id_demande = row['id_demande'], mail = row['mail'], isSending=row['issending'])
                demandes.append(demande)

        return demandes