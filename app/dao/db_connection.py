import os
import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor


class DBConnection:
    """
    Technical class to open only one connection to the DB.
    """
    def __init__(self):
        dotenv.load_dotenv(override=True)
        # Open the connection. 
        self.__connection =psycopg2.connect(
            host=os.environ["BDD_HOST"],
            port=os.environ["BDD_PORT"],
            database=os.environ["DATABASE"],
            user=os.environ["USER"],
            password=os.environ["BDD_PASSWORD"],
            cursor_factory=RealDictCursor)

    @property
    def connection(self):
        """
        return the opened connection.

        :return: the opened connection.
        """
        return self.__connection
