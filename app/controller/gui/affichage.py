from tkinter import *
import re
import datetime

from model.demande_service import DemandeService
from model.demande import Demande


class Affichage:
	"""Permet l'affichage d'une fenêtre pour ajouter une demande d'envoi d'un mail à une heure prévue"""

	def __init__(self):
		self.root = Tk()
		self.root.title('E-mail')
		self.root.resizable(False, False)
		self.root.geometry('480x360')
		self.up_frame = Frame(self.root)

		self.compose_frame = Frame(self.root)
		self.compose_frame.pack(side=TOP, expand=NO, fill=NONE)

		self.create_compose_widget()

		self.frame_when = Frame(self.root)
		self.frame_when.pack(side=TOP, expand=NO, fill=NONE)

		self.create_when_widget()

		self.down_frame = Frame(self.root)
		self.down_frame.pack(side=TOP, expand=NO, fill=NONE)

		self.create_down_widget()



	def create_compose_widget(self):
		"""Créer les différents widgets qui permettent de composer le mail"""
		Label(self.compose_frame,width=20,text="Compose ton Mail",
			  font=("Helvetica 17 bold")).grid(row=0, columnspan=3, pady=10)

		Label(self.compose_frame, text="            À : ").grid(row=1, sticky=E, pady=5)
		self.E_to = Entry(self.compose_frame, width=30, font=("Helvetica 17"))
		self.E_to.grid(row=1, column=1, pady=5)


	def create_when_widget(self):
		"""Créer les widgets pour entrer la date d'envoi du mail"""
		Label(self.frame_when, text="  Quand : ").grid(row=0, column=0, sticky=E, pady=5)

		self.day_when = Entry(self.frame_when, width=4, font=("Helvetica 17"))
		self.day_when.insert(END, 'jour')
		self.day_when.grid(row=0, column=1, sticky=E, pady=5)

		self.month_when = Entry(self.frame_when, width=4, font=("Helvetica 17"))
		self.month_when.insert(END, 'mois')
		self.month_when.grid(row=0, column=2, sticky=E, pady=5)

		self.year_when = Entry(self.frame_when, width=8, font=("Helvetica 17"))
		self.year_when.insert(END, 'année')
		self.year_when.grid(row=0, column=3, sticky=E, pady=5)

		self.heure_when = Entry(self.frame_when, width=4, font=("Helvetica 17"))
		self.heure_when.insert(END, 'h')
		self.heure_when.grid(row=0, column=4, sticky=E, pady=5)

		self.minute_when = Entry(self.frame_when, width=4, font=("Helvetica 17"))
		self.minute_when.insert(END, 'min')
		self.minute_when.grid(row=0, column=5, sticky=E, pady=5)


	def create_down_widget(self):
		"""Créer les widgets en dessous de la date"""
		self.E_subject = Entry(self.down_frame, width=30, font=("Helvetica 17"))
		self.E_message = Text(self.down_frame, width=30, height=5, font=("Helvetica 17"), 
							  highlightbackground='white', highlightthickness=1)

		Label(self.down_frame, text="Objet : ").grid(row=3, sticky=E, pady=5)
		Label(self.down_frame, text="Message : ").grid(row=4, sticky=E)

		self.E_subject.grid(row=3, column=1, pady=5)
		self.E_message.grid(row=4, column=1, pady=5)

		send = Button(self.down_frame, text="Envoyé", width=10,bg="#ffcccc", fg="black", command=self.send_mail)
		send.grid(row=6, columnspan=3, pady=5)

		self.error = Label(self.down_frame,width=40,fg="red",font=("Helvetica 15 bold"))
		self.error.grid(row=7, columnspan=3)


	def check_email(self):
		"""Méthode pour examiner l'e-mail du recepteur. 
		Si la forme n'est pas conforme alors la demande ne sera pas envoyée
		
		Returns
		-------
		bool
		"""
		receiver_email = str(self.E_to.get())
		EMAIL_REGEX = re.compile(r"[^@\s]+@[^@\s]+\.[a-zA-Z0-9]+$")
		if not EMAIL_REGEX.match(receiver_email):
			return False
		else:
			return True


	def check_date(self):
		"""Méthode pour examiner la date fournie. 
		Si la date n'est pas postérieure ou n'est pas valide alors la demande ne sera pas envoyée
		
		Returns
		-------
		bool
		"""
		try:
			day = int(self.day_when.get())
			month = int(self.month_when.get())
			year = int(self.year_when.get())
			hour = int(self.heure_when.get())
			minutes = int(self.minute_when.get())
			sending_date = datetime.datetime(year, month, day, hour, minutes)
			if sending_date < datetime.datetime.now():
				return False
			else:
				return True
		except ValueError:
			return False


	def check_messages(self):
		"""Méthode pour examiner l'objet et le contenu du mail fournis. 
		Si le contenu ou l'objet est vide alors la demande ne sera pas envoyée
		
		Returns
		-------
		bool
		"""
		subject = str(self.E_subject.get())
		message = str(self.E_message.get('1.0', END))
		if subject == "" or len(message) == 1:
			return False
		else:
			return True
			

	def send_mail(self):
		"""Méthode qui renvoie la demande si les différentes informations requises sont fournies. 
		Affiche également l'identifiant de la demande pour pouvoir suivre sa demande
		
		Returns
		-------
		Demande
		"""
		if not self.check_email():
			self.down_frame.pack()
			self.error.grid()
			self.error['text'] = "Entrer un e-mail valide !"
			self.root.after(1000, self.error.grid_remove)
			return None
		elif not self.check_date():
			self.down_frame.pack()
			self.error.grid()
			self.error['text'] = "Entrer une date valide !"
			self.root.after(1000, self.error.grid_remove)
			return None
		elif not self.check_messages():
			self.down_frame.pack()
			self.error.grid()
			self.error['text'] = "Remplisser l'objet et le message"
			self.root.after(2000, self.error.grid_remove)
			return None
		else:
			day = int(self.day_when.get())
			month = int(self.month_when.get())
			year = int(self.year_when.get())
			hour = int(self.heure_when.get())
			minutes = int(self.minute_when.get())
			sending_date = datetime.datetime(year, month, day, hour, minutes)
			sending_date = sending_date.strftime("%a %b %d %H:%M:%S %Y")
			mail = {"destinataire": str(self.E_to.get()), 
					"Subject": str(self.E_subject.get()), 
					"Contenu": str(self.E_message.get('1.0', END)), 
					"date-envoi": sending_date}

			demande = DemandeService().create_demande(Demande(mail=mail, isSending=False))

			little_window = Tk()
			little_window.geometry('240x100')
			little_window.title("L'identifiant de votre demande")
			little_window.resizable(False, False)
			Label(little_window, text="L'identifiant de votre demande est " + str(demande.id_demande)).pack()
			little_window.mainloop()

			return demande



if __name__ == '__main__':
	affiche = Affichage()

	affiche.root.mainloop()