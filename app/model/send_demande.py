from model.demande_service import DemandeService
from model.demande import Demande

from smtplib import SMTP

import datetime
import os
import dotenv


class SendDemande:
    def __init__(self):
        dotenv.load_dotenv(override=True)
        self.server = os.environ['SMTP_HOST']
        self.port = os.environ['SMTP_PORT']


    def checking_date(self):
        """Vérifie si la date des demandes non envoyées sont bien passées. 
        Si tel est le cas alors le mail est envoyé et la demande est modifiée
        """
        now = datetime.datetime.now()
        demandes = DemandeService().get_demande_not_send()

        nb_mail_send = 0

        for demande in demandes:
            sending_date = datetime.datetime.strptime(demande.mail['date-envoi'], "%a %b %d %H:%M:%S %Y")
            if sending_date < now:
                nb_mail_send += self.send_mail(demande)

                DemandeService().send_demande(demande.id_demande)
        
        return nb_mail_send


    def send_mail(self, demande: Demande):
        """Méthode qui envoie le mail grâce au service SMTP
        
        Parameters
        ----------
        demande: Demande dont le mail va être envoyer
            Demande
        """
        receiver = demande.mail['destinataire']
        sender = os.environ['USER_EMAIL']
        subject = demande.mail['Subject']

        header = 'To:' + receiver + '\n' + 'From: ' + sender + '\n' + 'Subject:' + subject + ' \n'
        message = header + '\n' + demande.mail['Contenu'] + '\n'

        with SMTP(self.server, self.port) as smtp:
            if not self.server == 'localhost': # Il faut s'identifier au prêt du serveur SMTP
                smtp.login(sender, os.environ['USER_PASSWORD'])

            try:
                smtp.sendmail(sender, receiver, message)
                return True
            except:
                print("La demande avec l'id: " + str(demande.id_demande) + " n'a pas pu être envoyée")
                return False

