import requests

from model.demande import Demande

host = "http://127.0.0.1:8000"

class DemandeService:
    """Ensemble des méthodes pour requêter l'API"""

    def get_demandes(self) -> list[Demande]:
        """Méthode qui renvoie l'ensemble des demandes effectuées
        
        Returns
        -------
        list[Demande]
        """
        req = requests.get(f"{host}/demandes")

        demandes = []
        if req.status_code == 200:
            json_demandes = req.json()
            for json_demande in json_demandes:
                demande = Demande(id_demande = json_demande['id_demande'], 
                                  mail = json_demande['mail'], 
                                  isSending = json_demande['isSending'])
                demandes.append(demande)

        return demandes


    def create_demande(self, demande: Demande) -> Demande:
        """Méthode pour créer une demande de mail
        
        Parameters
        ----------
        demande: Demande à créée
            Demande
        
        Returns
        -------
        Demande
        """
        payload = {'mail': demande.mail, 'isSending': demande.isSending}

        req = requests.post(f"{host}/demande", json = payload)

        if req.status_code == 200:
            json_demande = req.json()
            demande = Demande(id_demande = json_demande['id_demande'], 
                              mail = json_demande['mail'], 
                              isSending = json_demande['isSending'])
        else:
            demande = None

        return demande


    def get_demande(self, id_demande: int) -> Demande:
        """Méthode pour récupérer une demande liée à son identifiant
        
        Parameters
        ----------
        id_demande: identifiant associé à la demande
            int
        
        Returns
        -------
        demande
        """
        req = requests.get(f"{host}/demande/{id_demande}")

        if req.status_code == 200:
            json_demande = req.json()
            demande = Demande(id_demande = json_demande['id_demande'], 
                              mail = json_demande['mail'], 
                              isSending = json_demande['isSending'])
        else:
            demande = None

        return demande


    def send_demande(self, id_demande: int) -> bool:
        """Méthode qui modifie la demande associée à l'identifiant car celle-ci a été envoyée. 
        Si la demande est bien modifiée alors True est renvoyéet False sinon
        
        Parameters
        ----------
        id_demande: identifiant associé à la demande
            int
        
        Returns
        -------
        bool
        """
        payload = {'isSending': True}

        req = requests.put(f"{host}/demande/{id_demande}", json = payload)

        updated = (req.status_code == 200)

        return updated


    def get_demande_not_send(self) -> list[Demande]:
        """Méthode qui récupère l'ensemble des demandes qui n'ont pas encore été envoyées
        
        Returns
        -------
        list[Demande]
        """
        params = {'isSending': False}
        req = requests.get(f"{host}/demandes", params = params)

        demandes = []
        if req.status_code == 200:
            json_demandes = req.json()
            for json_demande in json_demandes:
                demande = Demande(id_demande = json_demande['id_demande'], 
                                  mail = json_demande['mail'], 
                                  isSending = json_demande['isSending'])
                demandes.append(demande)

        return demandes