from pydantic import BaseModel

class Demande(BaseModel):
    """Définit les demandes qui sont stockées dans la base de données"""
    id_demande: int = None
    mail: dict
    isSending: bool
