import unittest
from unittest.mock import patch, MagicMock

from model.demande_service import DemandeService
from model.demande import Demande


class TestDemandeService(unittest.TestCase):
    @patch('model.demande_service.requests')
    def test_get_demande(self, mock_requests):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {'id_demande': 1, 
                                           'mail': {'test': 'test'}, 
                                           'isSending': True}

        mock_requests.get.return_value = mock_response

        self.assertEqual(DemandeService().get_demande(1).id_demande, 1)
        self.assertEqual(DemandeService().get_demande(1).mail, {'test': 'test'})
        self.assertTrue(DemandeService().get_demande(1))
    

    @patch('model.demande_service.requests')
    def test_get_demandes(self, mock_requests):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = [{'id_demande': 1, 'mail': {'test1': 'test1'}, 'isSending': True}, 
                                           {'id_demande': 2, 'mail': {'test2': 'test2'}, 'isSending': False}]
        
        mock_requests.get.return_value = mock_response

        self.assertEqual(len(DemandeService().get_demandes()), 2)
        self.assertEqual(DemandeService().get_demandes()[0], 
                         {'id_demande': 1, 'mail': {'test1': 'test1'}, 'isSending': True})
        self.assertEqual(DemandeService().get_demandes()[1], 
                         {'id_demande': 2, 'mail': {'test2': 'test2'}, 'isSending': False})


    @patch('model.demande_service.requests')
    def test_send_message(self, mock_requests):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.return_value = (mock_response.status_code == 200)

        mock_requests.put.return_value = mock_response

        self.assertEqual(DemandeService().send_demande(1), True)


    @patch('model.demande_service.requests')
    def test_create_demande(self, mock_requests):
        mock_response = MagicMock()
        mock_response.status_code = 200
        mock_response.json.return_value = {'id_demande': 1, 'mail': {'test': 'test'}, 'isSending': False}

        mock_requests.post.return_value = mock_response

        demande = Demande(id_demande=1, mail={'test': 'test'}, isSending=False)
        self.assertEqual(DemandeService().create_demande(demande), demande)



if __name__ == '__main__':
    unittest.main()