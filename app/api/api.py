from fastapi import FastAPI

from .router import demande


app = FastAPI()


app.include_router(demande.router)


@app.get("/")
async def load_app():
    return {'message': 'Hello this my API for the mail service'}
