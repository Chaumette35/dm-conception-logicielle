from typing import Union

from fastapi import APIRouter

from model.demande import Demande
from dao.demande_dao import DemandeDao


router = APIRouter()


@router.get("/demandes", tags = ["demandes"])
async def read_demandes(isSending: Union[bool, None] = None):
    if isSending == False:
        return DemandeDao().find_demande_not_send()
    else:
        return DemandeDao().find_demandes()
    


@router.get("/demande/{id_demande}", tags = ["demande"])
async def read_demande_id(id_demande: int):
    return DemandeDao().find_demande_id(id_demande)


@router.post("/demande", tags = ["demande"])
async def post_demande_id(demande: Demande):
    return DemandeDao().add_demande(demande)


@router.put("/demande/{id_demande}", tags = ["demande"])
async def send_demande(id_demande: int):
    return DemandeDao().send_demande(id_demande)