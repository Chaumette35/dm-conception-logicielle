CREATE SCHEMA IF NOT EXISTS conception_logicielle;

DROP TABLE IF EXISTS conception_logicielle.demande;

DROP SEQUENCE IF EXISTS conception_logicielle.sequence_demande;

CREATE TABLE conception_logicielle.demande(
    id_demande NUMERIC PRIMARY KEY, 
    mail JSON, 
    isSending boolean
);

CREATE SEQUENCE conception_logicielle.sequence_demande
START WITH 1
INCREMENT BY 1;
